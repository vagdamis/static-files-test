var ArcoLib = (function () {

    var self = {};

    var MutationObserver = window.MutationObserver || window.WebKitMutationObserver;


    /**
     * Environment info.
     * 
     * @type {string}
     * @readonly
     */
    self.env = PMDynaform.getEnvironment();

    /**
     * Check if an element is inside a form which is in view mode.
     * 
     * @param {JQuery<HTMLElement>} $el 
     */
    self.isInViewModeForm = function ($el) {
        return $el.closest('.pmdynaform-view-form').length > 0;
    }

    /**
     * Check if OS is Android.
     */
    self.isAndroid = function () {
        return self.env.indexOf('android') >= 0;
    }

    /**
     * Check if device used has a touch screen.
     */
    self.isTouchDevice = function () {
        return (('ontouchstart' in window)
            || (navigator.MaxTouchPoints > 0)
            || (navigator.msMaxTouchPoints > 0));
    }

    /**
     * Get date time string in he form 'd/m/Y H:M:S'.
     * 
     * @return {string}
     */
    self.getDateTime = function () {
        var now = new Date();
        var year = now.getFullYear();
        var month = now.getMonth() + 1;
        var day = now.getDate();
        var hour = now.getHours();
        var minute = now.getMinutes();
        var second = now.getSeconds();

        if (day < 10) day = '0' + day;
        if (month < 10) month = '0' + month;
        if (hour < 10) hour = '0' + hour;
        if (minute < 10) minute = '0' + minute;
        if (second < 10) second = '0' + second;

        return day + '/' + month + '/' + year + ' ' + hour + ':' + minute + ':' + second;
    }

    /**
     * A MutationObserver that listens for added elements.
     * 
     * @param {Function} callback The function to execute with a parameneter the node found.
     */
    self.AddedNodesObserver = function (callback) {
        return new MutationObserver(function (mutationRecords) {
            mutationRecords.forEach(function (mutation) {
                if (mutation.type == 'childList' && mutation.addedNodes) {
                    mutation.addedNodes.forEach(function (node) {
                        callback(node);
                    });
                }
            });
        });
    }

    /**
     * Check if an array has duplicates.
     * 
     * @param {array} array
     */
    self.hasDuplicates = function (array) {
        var valuesSoFar = Object.create(null);
        for (var i = 0; i < array.length; ++i) {
            var value = array[i];
            if (value in valuesSoFar) {
                return true;
            }
            valuesSoFar[value] = true;
        }
        return false;
    }

    /**
     * Get the indexes of the duplicate values in an array.
     * 
     * @param {array} array The indexes array.
     * @return {number[]}
     */
    self.getDuplicateIndexes = function (array) {
        var duplicates = [];
        for (var i = 0; i < array.length; i++) {
            var el = array[i];
            if (array.indexOf(el) != array.lastIndexOf(el)) {
                duplicates.push(i);
            }
        }
        return duplicates;
    }

    /**
     * Scroll to a JQuery element.
     * 
     * @param {JQuery<HTMLElement>} $el
     * @param {number=} dur
     */
    self.scrollToEl = function ($el, dur) {
        var d = dur === undefined ? 600 : dur;
        $([document.documentElement, document.body]).animate({
            scrollTop: $el.offset().top - 50
        }, d);
    }

    /**
     * Get the string with italics tag between its first and last parenthesis.
     * 
     * @param {string} text
     * @return {string}
     */
    self.addItalicsInParenthesis = function (text) {
        if (!text) return '';

        var openPar = text.indexOf('(');
        var closePar = text.lastIndexOf(')');

        if (openPar >= 0 && closePar > 0 && openPar < closePar) {
            return text.substr(0, openPar + 1) + '<i>' +
                text.substr(openPar + 1, closePar - openPar - 1) + '</i>' +
                text.substr(closePar, text.length - closePar);
        }

        return text;
    }

    /**
     * Get the string with italics tag between its parenthesis.
     * 
     * @param {JQuery<HTMLElement>} $el
     */
    self.addItalicsInParenthesisInEl = function ($el) {
        if (!$el || $el.length <= 0) return;
        var text = $el.html();
        if (!text) return;

        var newText = self.addItalicsInParenthesis(text);
        if (newText !== text) {
            $el.html(newText);
        }
    }

    /**
     * Validate a grid that should have unique values in row columns.
     * 
     * @param {JQuery<HTMLElement>} $grid The PM grid control to validate.
     * @param {number[]} colIndexes The indexes of the columns to check, starting from 1.
     * @return {boolean}
     */
    self.validateGridUniqueRows = function ($grid, colIndexes) {
        var errClass = 'has-error';
        var $rows = $grid.find('.pmdynaform-grid-row');
        var $title = $grid.find('.pmdynaform-grid-title');
        $title.removeClass(errClass);
        $rows.removeClass(errClass);

        var vals = [];
        var numRows = $grid.getNumberRows();
        if (numRows <= 1) return true;
        for (var i = 1; i <= numRows; i++) {
            var rowVals = [];
            colIndexes.forEach(index => {
                rowVals.push($grid.getValue(i, index));
            });
            vals.push(JSON.stringify(rowVals));
        }
        var dups = self.getDuplicateIndexes(vals);
        if (dups.length > 0) {
            $title.addClass(errClass);
            $rows.filter(function (index) {
                return dups.indexOf(index) >= 0;
            }).addClass(errClass);
            return false;
        }
        return true;
    }

    /**
     * Get the datepicker element inside a grid.
     * 
     * @param {JQuery<HTMLElement>} $grid
     * @param {number} row
     * @param {number} col
     * @return {JQuery<HTMLElement>}
     */
    self.getGridDatepickerEl = function ($grid, row, col) {
        var $ctrl = $grid.getControl(row, col);
        return $ctrl.length ? $ctrl.parent('.date') : $([]);
    }

    /**
     * Set the min date of a datepicker element.
     * 
     * @param {JQuery<HTMLElement>} $datePicker
     * @param {*} dt
     */
    self.datepickerMinDate = function ($datePicker, dt) {
        $datePicker.data("DateTimePicker").minDate(dt);
    }

    /**
     * Set the max date of a datepicker element.
     * 
     * @param {JQuery<HTMLElement>} $datePicker
     * @param {*} dt
     */
    self.datepickerMaxDate = function ($datePicker, dt) {
        $datePicker.data("DateTimePicker").maxDate(dt);
    }

    /**
     * Add min and max dates in grid datepickers.
     * 
     * @param {JQuery<HTMLElement>} $grid The PM grid control to use.
     * @param {number} colIndex The index of the column that has the datepicker.
     * @param {*} minDate The moment element with min date.
     * @param {*} maxDate The moment element with max date.
     */
    self.setGridDatepickersLimits = function ($grid, colIndex, minDate, maxDate) {
        // Set min and max dates on existing datepickers.
        if (!$grid.length) return;
        var numRows = $grid.getNumberRows();
        for (var i = 1; i <= numRows; i++) {
            var $datePicker = self.getGridDatepickerEl($grid, i, colIndex);
            if (!$datePicker.length) continue;
            if (minDate)
                self.datepickerMinDate($datePicker, minDate);
            if (maxDate)
                self.datepickerMaxDate($datePicker, maxDate);
        }
    }

    /**
     * Add title above sitnature.
     * 
     * @param {JQuery<HTMLElement>} $el The signature element.
     * @param {string} title The title of the signature..
     * @param {string=} extraClass The extra class to add in the title element.
     */
    self.addTitleToElement = function ($el, title, extraClass) {
        var $title = $('<h3 class="panel-col-title lower"></h3>');
        if (extraClass) $title.addClass(extraClass);
        $title.html(title);
        $el.prepend($title);
    }

    /**
     * Save form draft.
     * 
     * @param {JQuery<HTMLElement>} $form The form element.
     * @param {string=} popupText The text to show on the save popup.
     */
    self.saveDraft = function ($form, popupText) {
        if (self.isAndroid()) {
            $form.saveForm();
            alert(popupText || 'Draft Saved');
        }
        else {
            $form.showFormModal();
            $form.saveForm();
            $form.hideFormModal();
        }
    }

    /**
     * Add a click event to a button to save the form draft.
     * 
     * @param {JQuery<HTMLElement>} $button The form element.
     * @param {JQuery<HTMLElement>} $form The form element.
     * @param {string=} popupText The text to show on the save popup.
     */
    self.addSaveDraftClick = function ($button, $form, popupText) {
        $button.click(function () {
            self.saveDraft($form, popupText);
        });
    }

    /** 
     * @param {string} pCode
     */
    self.validateProjectCode = function (pCode) {
        return !!pCode.match(/^[0-9A-Za-z]{5}$/g)
    }

    /** 
     * @param {string} uID
     */
    self.validateUserID = function (uID) {
        return !!uID.match(/^[0-9A-Fa-f]{32}$/g)
    }

    /**
     * Validate progect suggest widget which has value the project code and test the project title.
     * 
     * @param {JQuery<HTMLElement>} $el The suggest PM element.
     * @param {boolean=} sameTextAndValue If the value and text of the element should be the same. Default false.
     */
    self.validateProjectSuggest = function ($el, sameTextAndValue) {
        /** @type {string} */
        var code = $el.getValue();
        /** @type {string} */
        var text = $el.getText();
        return code.length && text.length && self.validateProjectCode(code) &&
            (sameTextAndValue ? code === text : code !== text);
    }

    /**
     * Validate progect suggest widget which has value the project code and test the project title.
     * If invalid, add error class to element.
     * 
     * @param {JQuery<HTMLElement>} $el The suggest PM element.
     * @param {boolean=} sameTextAndValue If the value and text of the element should be the same. Default false.
     * @param {string=} errorClass The error class to use. Default 'has-error'.
     */
    self.validateProjectSuggestWithError = function ($el, sameTextAndValue, errorClass) {
        if (!errorClass) errorClass = 'has-error'
        var isValid = self.validateProjectSuggest($el, sameTextAndValue);
        isValid ?
            $el.removeClass(errorClass) :
            $el.addClass(errorClass);
        return isValid
    }

    /**
     * Validate user suggest widget which has the user ID as value and user info as text.
     * 
     * @param {JQuery<HTMLElement>} $el The user PM element.
     */
    self.validateUserSuggest = function ($el) {
        /** @type {string} */
        var id = $el.getValue();
        /** @type {string} */
        var text = $el.getText();
        return id.length && text.length && id !== text &&
            self.validateUserID(id)
    }

    /**
     * Validate user suggest widget which has the user ID as value and user info as text.
     * If invalid, add error class to element.
     * 
     * @param {JQuery<HTMLElement>} $el The user PM element.
     * @param {string=} errorClass The error class to use. Default 'has-error'.
     */
    self.validateUserSuggestWithError = function ($el, errorClass) {
        if (!errorClass) errorClass = 'has-error'
        var isValid = self.validateUserSuggest($el);
        isValid ?
            $el.removeClass(errorClass) :
            $el.addClass(errorClass);
        return isValid
    }

    /**
     * Disable validation for all form fields (not grid columns).
     * 
     * @param {JQuery<HTMLElement>} $form 
     */
    self.disableFormValidation = function ($form) {
        var fields = $form.getFields();
        for (var i = 0; i < fields.length; i++) {
            fields[i].disableValidation();
        }
    }

    /**
     * Set a grid to be empty by default.
     * 
     * @param {JQuery<HTMLElement>} $grid 
     */
    self.emptyGridByDefault = function ($grid) {
        var data = $grid.getInfo().data;
        if ($grid.getNumberRows() == 1 && (data.length === 0 || data[0] != undefined)) {
            $grid.deleteRow();
        }
    }

    /**
     * Get the content of the distribution list popup as a jquery element.
     * 
     * @return {JQuery<HTMLElement>} 
     */
    self.getDistributionListPreviewContent = function () {
        return $(
            '<div class="distr-list-preview-content"> \
                <div class="distr-list-preview-error"></div> \
                <table class="distr-list-preview-table"> \
                    <thead> \
                        <tr> \
                            <th>Type</th> \
                            <th>Dep/Div/Reg</th> \
                            <th>Job Position</th> \
                            <th>Name</th> \
                            <th>Email</th> \
                        </tr> \
                    </thead> \
                    <tbody></tbody> \
                </table> \
            </div>'
        );
    }

    /**
     * Change the look of a button to red
     * 
     * @param {JQuery<HTMLElement>} $pmButton
     */
    self.redButtonTheme = function ($pmButton) {
        $pmButton.find('i').remove();

        $pmButton.find("button")
            .removeClass("button-blue")
            .removeClass("button-green")
            .addClass("button-red")
            .prepend("<i class='fa fa-times'></i>");
    }

    /**
     * Add autosave feature in form. Shows message on save.
     * 
     * @param {JQuery<HTMLElement} $form 
     * @param {Number} saveEveryMS 
     */
    self.addAutosave = function($form, saveEveryMS = 600000) {
        var $autosaveMessage = $(
            '<div class="autosave-message"> \
                <div class="autosave-loader"></div> \
                <span>Saving form...</span> \
            </div>'
        );

        $('body').append($autosaveMessage);

        setInterval(function () {
            try {
                $form.saveForm();
            } catch (error) {
                console.error(error);
            }
            $autosaveMessage.addClass("slidein").delay(1500).queue(function(){
                $autosaveMessage.removeClass('slidein').dequeue();
            });
        }, saveEveryMS);
    }

    return self;

})();


$(document).ready(function () {

    // Define mutation observer
    var MutationObserver = window.MutationObserver || window.WebKitMutationObserver;

    // End Signature Date Update
    (function () {

        // Initialize dates in signatures
        $(".form-signature .signature-date").each(function () {
            var $el = $(this);
            var hiddenElId = $el.attr('_for');
            var hiddenEl = $('#' + hiddenElId);
            if (hiddenEl.length > 0) {
                var dt = hiddenEl.getValue();
                $el.html(dt);
            }

            if (!$el.hasClass('dynamic') && $el.text() && !$el.find('.signature-timezone').length) {
                $el.append('<span class="signature-timezone">UTC</span>');
            }
        });

        var dynSygnatures = $(".form-signature .signature-date.dynamic");

        // Update dynamic dates in signatures
        if (dynSygnatures.length > 0) {
            setInterval(function () {

                var currentTime = ArcoLib.getDateTime();

                dynSygnatures.each(function () {
                    var $el = $(this);

                    if (ArcoLib.isInViewModeForm($el)) {
                        return;
                    }

                    var hiddenElId = $el.attr('_for');
                    var hiddenEl = $('#' + hiddenElId);

                    if (hiddenEl.length > 0) {
                        hiddenEl.setValue(currentTime);
                        $el.html(currentTime);
                    }
                });
            }, 1000);
        }
    })();

    // End Signature Date Update

    // Toggle Buttons Table
    $('.toggle-buttons-table').each(function () {
        var $table = $(this);
        var $buttons = $table.find('.table-button');
        var $hiddenCheckGroup = $('#' + $table.attr('_for'));

        /** @type {string[]} */
        var checkedVals = $hiddenCheckGroup.getValue();

        $buttons.each(function (i, button) {
            var $btn = $(button);
            var val = $btn.attr('_val');
            checkedVals.indexOf(val) >= 0 ?
                $btn.addClass('active') :
                $btn.removeClass('active');
        })

        if (ArcoLib.isTouchDevice()) {
            $table.addClass('touch-mode');
        }

        // If in view mode, dont do anything.
        if (ArcoLib.isInViewModeForm($table)) {
            $table.addClass('view-mode');
            return false;
        }

        if ($table.hasClass('required') && $table.attr('_form_id')) {
            var $form = $('#' + $table.attr('_form_id'));
            var $container = $table.closest('.toggle-buttons-table-cont');
            var valText = 'This field is required.';
            var $valMessage = $container.find('.validation-message');

            if ($form.length > 0) {
                $form.setOnSubmit(function () {
                    if ($table.find('span.active').length == 0) {
                        $container.addClass('invalid');
                        if (!$valMessage.text()) {
                            $valMessage.text(valText);
                        }
                        $([document.documentElement, document.body]).animate({
                            scrollTop: $table.offset().top
                        }, 400);
                        return false;
                    } else {
                        $container.removeClass('invalid');
                        return true;
                    }
                });
            }
        }

        $buttons.click(function () {
            var $el = $(this);

            var val = $el.attr('_val');
            /** @type {string[]} */
            var checkedVals = $hiddenCheckGroup.getValue();
            /** @type {string[]} */
            var checkedTexts = JSON.parse($hiddenCheckGroup.getText());

            if ($el.hasClass('active')) {
                var newTexts = []
                checkedVals.forEach(function (v, i) {
                    if (v != val)
                        newTexts.push(checkedTexts[i])
                })
                $hiddenCheckGroup.setText(newTexts);
                $el.removeClass('active');
            }
            else {
                if (checkedVals.indexOf(val) < 0) {
                    checkedTexts.push($el.text());
                    $hiddenCheckGroup.setText(checkedTexts);
                }
                $el.addClass('active');
            }
        });
    });

    // Movable Buttons
    $('.multi-select-lists').each(function () {
        var $widget = $(this);
        var $leftTable = $widget.find('.buttons-left .scroll-options');
        var $rightTable = $widget.find('.buttons-right .scroll-options');
        var $hiddenCheckGroup = $('#' + $widget.attr('_for'));

        if (!$hiddenCheckGroup.length) return;

        function getNewButton(text, val) {
            return $('<div></div>').attr('_val', val)
                .addClass('option').text(text);
        }

        // Create buttons from hidden checkgroup.
        if ($leftTable.find('.option').length == 0) {
            $hiddenCheckGroup.getControl().each(function () {
                var text = $(this).next().text();
                var val = $(this).val();

                $leftTable.append(getNewButton(text, val));
                $rightTable.append(getNewButton(text, val));
            });
        }

        var $leftButtons = $leftTable.find('.option');
        var $rightButtons = $rightTable.find('.option');

        /** @type {string[]} */
        var checkedVals = $hiddenCheckGroup.getValue();

        // Enable or disable buttons
        $leftButtons.each(function (i, button) {
            var $btn = $(button);
            var val = $btn.attr('_val');

            if (checkedVals.indexOf(val) >= 0) {
                $btn.addClass('active');
                $rightButtons.eq($btn.index()).addClass('active');
            }
            else {
                $btn.removeClass('active');
                $rightButtons.eq($btn.index()).removeClass('active');
            }
        });

        if (ArcoLib.isTouchDevice()) {
            $widget.addClass('touch-mode');
        }

        // If in view mode, dont do anything.
        if (ArcoLib.isInViewModeForm($widget)) {
            $widget.addClass('view-mode');
            return false;
        }

        if ($widget.attr('_form_id')) {
            var $form = $('#' + $widget.attr('_form_id'));
            var valText = 'This field is required.';
            var $valMessage = $widget.find('.validation-message');

            if ($form.length > 0) {
                $form.setOnSubmit(function () {
                    if (!$widget.hasClass('required')) return true;
                    if ($rightTable.find('.option.active').length == 0) {
                        $widget.addClass('invalid');
                        if (!$valMessage.text()) {
                            $valMessage.text(valText);
                        }
                        $([document.documentElement, document.body]).animate({
                            scrollTop: $widget.offset().top
                        }, 400);
                        return false;
                    } else {
                        $widget.removeClass('invalid');
                        return true;
                    }
                });
            }
        }

        $leftButtons.click(function () {
            var $btn = $(this);
            if ($btn.hasClass('active')) return;

            var val = $btn.attr('_val');
            /** @type {string[]} */
            var checkedVals = $hiddenCheckGroup.getValue();
            /** @type {string[]} */
            var checkedTexts = JSON.parse($hiddenCheckGroup.getText());

            if (checkedVals.indexOf(val) < 0) {
                checkedTexts.push($btn.text());
                $hiddenCheckGroup.setText(checkedTexts);
            }

            $btn.addClass('active');

            $rightButtons.eq($btn.index()).addClass('active');
        });

        $rightButtons.click(function () {
            var $btn = $(this);
            if (!$btn.hasClass('active')) return;

            var val = $btn.attr('_val');

            /** @type {string[]} */
            var checkedVals = $hiddenCheckGroup.getValue();
            /** @type {string[]} */
            var checkedTexts = JSON.parse($hiddenCheckGroup.getText());

            var newTexts = []
            checkedVals.forEach(function (v, i) {
                if (v != val)
                    newTexts.push(checkedTexts[i])
            })
            $hiddenCheckGroup.setText(newTexts);

            $btn.removeClass('active');

            $leftButtons.eq($btn.index()).removeClass('active');
        });

        $widget.find('.select-all').click(function () {
            $leftButtons.each(function (i, b) {
                if (!$(b).hasClass('active')) {
                    $(b).click();
                }
            });
        });

        $widget.find('.deselect-all').click(function () {
            $rightButtons.each(function (i, b) {
                if ($(b).hasClass('active')) {
                    $(b).click();
                }
            });
        });
    });
    // End Toggle Buttons Table

    // File Upload Element
    /**
     * @param {JQuery<HTMLElement>} $btn 
     */
    function editUploadButtonText($btn) {
        var text = $btn.text();
        var index = text.indexOf(':');
        if (index >= 0 && text.length > 1) {
            text = text.substr(index + 1, text.length).trim();
            var newText = 'Upload ' + (text ? '(' + text + ')' : '');
            $btn.text(newText);
        }
    }

    $('button.pmdynaform-control-file.form-control').each(function () {
        editUploadButtonText($(this));
    });

    var fileUploadButtonsObserver = ArcoLib.AddedNodesObserver(function (node) {
        var $node = $(node);
        if ($node.hasClass('pmdynaform-grid-row')) {
            $node.find("button.pmdynaform-control-file").each(function () {
                editUploadButtonText($(this));
            });
        }
    });

    $('.pmdynaform-grid-tbody').each(function () {
        fileUploadButtonsObserver.observe(this, { childList: true, subtree: true });
    });

    // End File Upload Element

    // Squared Radio Buttons

    /**
     * @param {JQuery<HTMLElement>} $buttons 
     * @param {string} value 
     */
    function setSquaredRadioButtonsValue($buttons, value) {
        $buttons.find('span.active').removeClass('active');
        $buttons.find('input[type=radio]').each(function () {
            var $radio = $(this);
            if ($radio.val() == value) {
                $radio.next('span').addClass('active');
            }
        });

        $buttons.removeClass('invalid');
    }

    $('.squared-radio-buttons').each(function () {
        var $buttons = $(this);

        setSquaredRadioButtonsValue($buttons, $buttons.getValue());

        // If in edit mode
        if (!ArcoLib.isInViewModeForm($buttons)) {
            $buttons.setOnchange(function (newVal, oldVal) {
                setSquaredRadioButtonsValue($buttons, newVal);
            });
        }
    });

    // End Squared Radio Buttons

    // Theme
    // edit mode
    if (!$(".pmdynaform-view-form").length) {
        /*$('input,textarea').on("change paste keyup", function () {
            if ($(this).val())
                $(this).addClass('valid-field');
            else
                $(this).removeClass('valid-field');
        });*/

        $('<span class="highlight"></span><span class="bar"></span>').insertAfter(".pmdynaform-control-text");
        $('<span class="highlight"></span><span class="bar"></span>').insertAfter(".pmdynaform-control-suggest");

        $(".pmdynaform-view-label div").removeClass(function (index, css) {
            return (css.match(/\bcol-\S+/g) || []).join(' ');
        }).addClass("col-xs-12 col-sm-12 col-md-12 col-lg-12");

        $('.pmdynaform-edit-text .pmdynaform-field-control').each(function () {
            var label_elem = $(this).parent().find('.pmdynaform-label');
            var parent_elem = label_elem.parent();
            label_elem.insertBefore($(this).find('.pmdynaform-control-text'));
            label_elem.addClass('textfield-label');
            parent_elem.remove();
        });

        $('.pmdynaform-field-suggest .pmdynaform-field-control').each(function () {
            var label_elem = $(this).parent().find('.pmdynaform-label');
            var parent_elem = label_elem.parent();
            label_elem.insertBefore($(this).find('.pmdynaform-control-suggest'));
            label_elem.addClass('textfield-label');
            parent_elem.remove();
        });

        $('.pmdynaform-field-textarea .pmdynaform-field-control').each(function () {
            var label_elem = $(this).parent().find('.pmdynaform-label');
            var parent_elem = label_elem.parent();
            label_elem.insertBefore($(this).find('.pmdynaform-control-textarea'));
            label_elem.addClass('textarea-label');
            parent_elem.remove();
        });

        $('.pmdynaform-field-datetime .pmdynaform-field-control').each(function () {
            var label_elem = $(this).parent().find('.pmdynaform-label');
            var parent_elem = label_elem.parent();
            label_elem.insertBefore($(this).find('.datetime-container'));
            parent_elem.remove();
        });

        $('.pmdynaform-field-checkgroup .pmdynaform-field-control').each(function () {
            var label_elem = $(this).parent().find('.pmdynaform-label');
            var parent_elem = label_elem.parent();
            label_elem.prependTo($(this));
            label_elem.addClass('checkgroup-label');
            parent_elem.remove();
        });

        $('.pmdynaform-field-radio .pmdynaform-field-control').each(function () {
            var label_elem = $(this).parent().find('.pmdynaform-label');
            var parent_elem = label_elem.parent();
            label_elem.prependTo($(this));
            label_elem.addClass('radio-label');
            parent_elem.remove();
        });

        $('.pmdynaform-field-dropdown .pmdynaform-field-control').each(function () {
            var label_elem = $(this).parent().find('.pmdynaform-label');
            var parent_elem = label_elem.parent();
            label_elem.prependTo($(this));
            label_elem.addClass('dropdown-label');
            parent_elem.remove();
        });

        $('.pmdynaform-field-checkbox .pmdynaform-field-control').each(function () {
            var label_elem = $(this).parent().find('.pmdynaform-label');
            var parent_elem = label_elem.parent();
            var parent_label = $(this).find('label:first-child');
            parent_label.addClass('pure-material-checkbox');
            label_elem.insertAfter($(this).find('input:first-child'));
            label_elem.addClass('dropdown-label');
            parent_elem.remove();
        });

        $('.pmdynaform-grid-row .pmdynaform-control-checkbox').each(function () {
            var parent_label = $(this).parent();
            $('<span>').appendTo(parent_label);
            parent_label.addClass('pure-material-checkbox');
            var parent_cb = parent_label.parent();
            parent_cb.css({ 'padding': '0' });
        });

        $('.pmdynaform-control-checkgroup').each(function () {
            $(this).parent().addClass('pure-material-checkbox');
        });

        $(".pmdynaform-edit-text .pmdynaform-field-control").removeClass(function (index, className) {
            return (className.match(/col-\S+/g) || []).join(' ');
        });
        $(".pmdynaform-control-suggest .pmdynaform-field-control").removeClass(function (index, className) {
            return (className.match(/col-\S+/g) || []).join(' ');
        });
        $(".pmdynaform-field-textarea .pmdynaform-field-control").removeClass(function (index, className) {
            return (className.match(/col-\S+/g) || []).join(' ');
        });
        $(".pmdynaform-field-datetime .pmdynaform-field-control").removeClass(function (index, className) {
            return (className.match(/col-\S+/g) || []).join(' ');
        });
        $(".pmdynaform-field-checkgroup .pmdynaform-field-control").removeClass(function (index, className) {
            return (className.match(/col-\S+/g) || []).join(' ');
        });
        $(".pmdynaform-field-radio .pmdynaform-field-control").removeClass(function (index, className) {
            return (className.match(/col-\S+/g) || []).join(' ');
        });
        $(".pmdynaform-field-dropdown .pmdynaform-field-control").removeClass(function (index, className) {
            return (className.match(/col-\S+/g) || []).join(' ');
        });
        $(".pmdynaform-field-checkbox .pmdynaform-field-control").removeClass(function (index, className) {
            return (className.match(/col-\S+/g) || []).join(' ');
        });
        $(".pmdynaform-edit-text .pmdynaform-field-control").addClass('col-xs-12 col-sm-12 col-md-12 col-lg-12');
        $(".pmdynaform-field-suggest .pmdynaform-field-control").addClass('col-xs-12 col-sm-12 col-md-12 col-lg-12');
        $(".pmdynaform-field-textarea .pmdynaform-field-control").addClass('col-xs-12 col-sm-12 col-md-12 col-lg-12');
        $(".pmdynaform-field-datetime .pmdynaform-field-control").addClass('col-xs-12 col-sm-12 col-md-12 col-lg-12');
        $(".pmdynaform-field-checkgroup .pmdynaform-field-control").addClass('col-xs-12 col-sm-12 col-md-12 col-lg-12');
        $(".pmdynaform-field-radio .pmdynaform-field-control").addClass('col-xs-12 col-sm-12 col-md-12 col-lg-12');
        $(".pmdynaform-field-dropdown .pmdynaform-field-control").addClass('col-xs-12 col-sm-12 col-md-12 col-lg-12');
        $(".pmdynaform-field-checkbox .pmdynaform-field-control").addClass('col-xs-12 col-sm-12 col-md-12 col-lg-12');

        $('<span class="input-group-addon"></span>').prependTo($('.date'));
    }
    // view mode
    else {
        $('<span class="highlight"></span><span class="bar"></span>').insertAfter(".pmdynaform-control-text");
        $('<span class="highlight"></span><span class="bar"></span>').insertAfter(".pmdynaform-control-suggest");

        $(".pmdynaform-field-suggest div").removeClass(function (index, css) {
            return (css.match(/\bcol-\S+/g) || []).join(' ');
        }).addClass("col-xs-12 col-sm-12 col-md-12 col-lg-12");

        $(".pmdynaform-edit-text div").removeClass(function (index, css) {
            return (css.match(/\bcol-\S+/g) || []).join(' ');
        }).addClass("col-xs-12 col-sm-12 col-md-12 col-lg-12");

        $(".pmdynaform-field-radio div").removeClass(function (index, css) {
            return (css.match(/\bcol-\S+/g) || []).join(' ');
        }).addClass("col-xs-12 col-sm-12 col-md-12 col-lg-12");

        $(".pmdynaform-field-checkgroup div").removeClass(function (index, css) {
            return (css.match(/\bcol-\S+/g) || []).join(' ');
        }).addClass("col-xs-12 col-sm-12 col-md-12 col-lg-12");

        $(".pmdynaform-field-checkbox div").removeClass(function (index, css) {
            return (css.match(/\bcol-\S+/g) || []).join(' ');
        }).addClass("col-xs-12 col-sm-12 col-md-12 col-lg-12");

        $(".pmdynaform-field-text div").removeClass(function (index, css) {
            return (css.match(/\bcol-\S+/g) || []).join(' ');
        }).addClass("col-xs-12 col-sm-12 col-md-12 col-lg-12");

        $(".pmdynaform-field-textarea div").removeClass(function (index, css) {
            return (css.match(/\bcol-\S+/g) || []).join(' ');
        }).addClass("col-xs-12 col-sm-12 col-md-12 col-lg-12");

        $(".pmdynaform-field-datetime div").removeClass(function (index, css) {
            return (css.match(/\bcol-\S+/g) || []).join(' ');
        }).addClass("col-xs-12 col-sm-12 col-md-12 col-lg-12");

        $(".pmdynaform-grid-row").find("input.pmdynaform-control-text").after('<span class="highlight"></span><span class="bar"></span>');
        // $("#attachmentsGrid-body").find("input").prop("disabled", true);

        $("#part1_section_panel6").hide();
        $("#part2_section_panel22").hide();
    }
    // End Theme

    // Dropdown structure
    $(".pmdynaform-field-dropdown").children().removeClass(function (index, className) {
        return (className.match(/col-\S+/g) || []).join(' ');
    });
    $(".pmdynaform-field-dropdown").children().addClass('col-xs-12 col-sm-12 col-md-12 col-lg-12');
    // End Dropdown structure

    // Buttons
    // Add new on grid
    $('.pmdynaform-grid-newitem').each(function () {
        var $btn = $(this)

        if ($btn.hasClass('button-red') ||
            $btn.hasClass('button-blue') ||
            $btn.hasClass('button-green')) return;

        $btn.addClass('button button-blue');
        $btn.find("span.glyphicon").remove();
        $btn.prepend("<i class='fa fa-plus'></i>");
    });

    // Primary form button
    $('.btn-primary').each(function () {
        var $btn = $(this)

        if ($btn.hasClass('button-red') ||
            $btn.hasClass('button-blue') ||
            $btn.hasClass('button-green')) return;

        $btn.addClass('button button-blue');
        $btn.find("span.glyphicon").remove();
        $btn.prepend("<i class='fa fa-level-up'></i>");
    });

    // Secondary form button
    $('.btn-default').each(function () {
        var $btn = $(this)

        if ($btn.hasClass('button-red') ||
            $btn.hasClass('button-blue') ||
            $btn.hasClass('button-green')) return;

        $btn.addClass('button button-green');
        $btn.find("span.glyphicon").remove();
        $btn.prepend("<i class='fa fa-floppy-o'></i>");
    });
    // End Buttons

    // Make Sections
    var cntr = 0;
    $('*[class^="start-section"]').each(function () {
        ++cntr;
        $(this).removeClass("start-section");
        $(this).addClass("start-section-" + cntr);
    });

    var cntr = 0;
    $('*[class^="end-section"]').each(function () {
        ++cntr;
        $(this).removeClass("end-section");
        $(this).addClass("end-section-" + cntr);
    });

    $('*[class^="start-section-"]').each(function () {
        sectionsCounter = $(this).attr('class').split('-').pop();
        $(".start-section-" + sectionsCounter).parent().parent().parent().parent().addClass("secstart-" + sectionsCounter);
        $(".end-section-" + sectionsCounter).parent().parent().parent().parent().addClass("secend-" + sectionsCounter);
        $(".secstart-" + sectionsCounter).nextUntil(".secend-" + sectionsCounter).addClass("sec-" + sectionsCounter);
        $(".sec-" + sectionsCounter).wrapAll("<div class='section-box' />");
        $(this).parent().parent().parent().parent().remove();
    });
    // End Make Sections

    // jQuery mutators for input fields highlight effect for dynamically added rows
    var targetNodes = $(".pmdynaform-grid-fields");
    var myObserver = new MutationObserver(mutationHandler);
    var obsConfig = { childList: true, characterData: true, attributes: true, subtree: true };

    // Add a target node to the observer. Can only add one node at a time.
    targetNodes.each(function () {
        myObserver.observe(this, obsConfig);
    });

    function mutationHandler(mutationRecords) {
        mutationRecords.forEach(function (mutation) {
            $(mutation.addedNodes[0]).find("input.pmdynaform-control-text").after('<span class="highlight"></span><span class="bar"></span>');
            $(mutation.addedNodes[0]).find("input.pmdynaform-control-suggest").after('<span class="highlight"></span><span class="bar"></span>');
            var dateElem = $(mutation.addedNodes[0]).find(".date").children().first().find("span.input-group-addon");
            if (!dateElem.length) {
                var $date = $(mutation.addedNodes[0]).find(".date");
                if ($date.length && !$date.find('.input-group-addon').length) {
                    $date.prepend('<span class="input-group-addon"></span>');
                }
            }
            myObserver.disconnect();

            targetNodes.each(function () {
                myObserver.observe(this, obsConfig);
            });
        });
    }
    // End jQuery mutators

    // Progress Indicator Bar
    if (!$(".progress-header").length) {
        $('body').before('<div class="progress-header">\
            <div class="progress-container">\
            <div class="progress-bar" id="progressIndicatorBar"></div>\
            </div>\
            </div>');
        $('body').css({ "margin-top": "10px" });
    }

    $(document).ready(function () {
        window.onscroll = function () { progressIndicator() };

        function progressIndicator() {
            var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
            var height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
            var scrolled = (winScroll / height) * 100;
            document.getElementById("progressIndicatorBar").style.width = scrolled + "%";
        }
    });
    // End Progress Indicator Bar

    // Add hovered state on controls on complaints
    $(document).on("mouseover focus", "#complaints input.form-control, #complaints textarea.form-control", function () {
        $("#complaints input.form-control").removeClass("hovered");
        $("#complaints textarea.form-control").removeClass("hovered");
        $(this).addClass("hovered");
    });
    $(document).on("mouseout", "#complaints input.form-control, #complaints textarea.form-control", function () {
        $("#complaints input.form-control").removeClass("hovered");
        $("#complaints textarea.form-control").removeClass("hovered");
        $(this).removeClass("hovered");
    });

    $("#complaints input.form-control, #complaints textarea.form-control").keydown(function (e) {
        var code = e.keyCode || e.which;
        if (code === 9) {
            $("#complaints input.form-control").removeClass("hovered");
            $("#complaints textarea.form-control").removeClass("hovered");
        }
    });
    // End Add hovered state on controls on complaints
});
